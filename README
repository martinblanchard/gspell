gspell - a spell-checking library for GTK+ applications
=======================================================

This is version 1.7.1 of gspell.

gspell provides a flexible API to add spell-checking to a GTK+ application.

The gspell library is free software and is released under the terms of the GNU
LGPL version 2.1 or later. See the file 'COPYING' for more information.

The official web site is:

    https://wiki.gnome.org/Projects/gspell

About versions
--------------

gspell follows the GNOME release schedule and uses the same versioning scheme:

    https://developer.gnome.org/programming-guidelines/stable/versioning.html.en

gspell 0.1.x should be installed alongside GNOME 3.18.
gspell 1.0.x should be installed alongside GNOME 3.20.
gspell 1.2.x should be installed alongside GNOME 3.22.
gspell 1.4.x should be installed alongside GNOME 3.24.
gspell 1.6.x should be installed alongside GNOME 3.26.

The 0.1 version didn't have a stable API, and there has been lots of API
changes between 0.1 and 1.0. The 0.1 and 1.0 versions are not completely
parallel-installable, only the libtool version has been bumped. Using the 0.1
version is highly discouraged.

Dependencies
------------

* GLib >= 2.44
* GTK+ >= 3.20
* Enchant >= 2.1.3
* iso-codes

Notes for packagers
-------------------

At least on GNU/Linux, it is better to install hunspell. aspell can be
considered deprecated and doesn't work well with gspell. If both hunspell and
aspell are installed, Enchant prefers hunspell.

See also:

    https://fedoraproject.org/wiki/Releases/FeatureDictionary
    https://wiki.ubuntu.com/ConsolidateSpellingLibs

Installation
------------

Simple install procedure from a tarball:

  $ ./configure
  $ make
  [ Become root if necessary ]
  $ make install

See the file 'INSTALL' for more detailed information.

From the Git repository, the 'configure' script and the 'INSTALL' file are not
yet generated, so you need to run 'autogen.sh' instead, which takes the same
arguments as 'configure'.

To build the latest version of gspell plus its dependencies from Git, Jhbuild
is recommended:

    https://wiki.gnome.org/Projects/Jhbuild

How to contribute
-----------------

See the file 'HACKING'.
